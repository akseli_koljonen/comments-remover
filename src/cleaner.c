#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "clean.h"
#include <signal.h>



void SigHandlerChild(int signo) {
if (signo == SIGINT) {
  printf("Painoit Ctr-c:");
  Log("Pressed Ctr-c: Children exiting");
  exit(1);
}
}

int cleanFile(char *filename) {
Log("Cleaning file started");
FILE *fr;
FILE *fw;

char *filecleaned = malloc(strlen(filename)+1+8);
if (filecleaned == NULL) {
  printf("Malloc failas");
  Log("Allocate memory failed");
  return -1;
}
filecleaned = strcpy(filecleaned,filename);
filecleaned = strcat(filecleaned,".cleaned");

fr = fopen(filename,"r");
if (fr == NULL) {
  printf("error in opening input file");
  Log("Error in opening Inputfile ");
}
fw = fopen(filecleaned,"w");
if (fw == NULL) {
  printf("error in opening output file");
  Log("Error in opening Outputfile ");
}

char c;
char ch;
c = fgetc (fr);
Log("Starting to read files");
while (c=='\n') { /* check if first line is empty*/
  c = fgetc(fr);
}

while (c != EOF)
{

   ch = c;
   c = fgetc (fr);

   if ((ch == '/') && (c == '*'))
   {
      ch = fgetc (fr);
      c = fgetc (fr);
      while (!((ch == '*') && (c == '/'))) /* unroll until the end of comment*/
      {
        ch = c;
        c = fgetc (fr);
      }
      c = fgetc (fr);
      c = fgetc (fr);
      Log("Deleting comment");
      continue;
   }
   else if((ch=='/') && (c == '/')) // block to handle single line comment.
   {
      c = fgetc (fr);
      while (!(c == '\n')){
         c = fgetc (fr);
      }
     c = fgetc (fr);
     continue;
   }

   else if ((ch == '\n') && (c == '\n'))  { /* unroll until the end of empty line*/
     Log("Deleting comment");
     continue;

   }
   else if ((ch == '\n') && (c == EOF)) { /* check if last line is empty*/
     Log("Deleting comment");
     continue;
   }

   putc (ch, fw);

 }

fclose (fr);
fclose (fw);

signal(SIGINT,SigHandlerChild);

  return 1;
}
