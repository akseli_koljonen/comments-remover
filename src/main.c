#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "clean.h"

#include <signal.h>


pid_t pid;
void SigHandlerParent(int signo) {
  waitpid(pid,NULL,0);
  Log("Pressed Ctr-c: Parent exiting");
  exit(1);
}
int ChildProcess(char * filename);



int main(int argc, char **argv)
{
Log("Program has started");

if (argc == 1) {
  Log("Not enough arguments. Program shutdown!");
  return 1;
}


char * filename = "";
int status = 0;
pid_t pid;
Log("Starting processing files ");
for (int i = 1; i < argc;i++) {

  pid = fork();
  if(pid < 0) {  // Check if Child forking is working
    Log("Child forking failed");
    printf("Error\n");
    exit(1);
  }
  filename = argv[i]; // Write filenames
  Log("Read filename");

  if (pid == 0) {
    Log("Child prosessed. Cleaning started");
    break;
}
}
if (pid==0) {
  printf("Hello I am the childprocess \n");
  printf("My pid is %d \n", getpid());
  printf("Filename is %s\n", filename);
  if(ChildProcess(filename)) {
    printf("Comments removed from the file %s\n",filename);
}
  else {
    printf("moi");
    Log("Child Processing failed.");
    return 1;
}
}
else {
waitpid(pid,NULL,0); // Wait for the childprosesses

}
}


int ChildProcess(char * filename) {

int cleanedFile;
cleanedFile = cleanFile(filename); // Start the cleaning

}
