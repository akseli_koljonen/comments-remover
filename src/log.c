#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>

static const char LOGFILE[] = "cleaning.log";
int LogCreated = 0;

void Log (char *message)
{
	FILE *file;

	if (!LogCreated) {
		file = fopen(LOGFILE, "w");
		LogCreated = 1;
	}
	else
		file = fopen(LOGFILE, "a");

	if (file == NULL) {
		LogCreated = 0;
	}
	else
	{
		fputs(message, file);
		fclose(file);
	}

}
